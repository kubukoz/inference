package com.kubukoz.inference

sealed abstract class Tree(left: Option[Tree], right: Option[Tree], operator: Option[Operator], symbol: Option[String]) {
  def toNodes: Seq[Node] = symbol match {
    case Some(name) => Node(name) :: Nil
    case None => List(left, right).flatten.flatMap(_.toNodes)
  }

  def evaluate(implicit known: Map[String, Boolean]): Option[Boolean]

  def symbols: List[String] = List(symbol.map(s => List(s)), left.map(_.symbols), right.map(_.symbols)).flatten.flatten
}

case class And(left: Tree, right: Tree) extends Tree(Some(left), Some(right), Some(AndOperator), None) {
  override def evaluate(implicit known: Map[String, Boolean]): Option[Boolean] = (left.evaluate zip right.evaluate).map {
    case (a, b) => a && b
  }.headOption
}

case class Or(left: Tree, right: Tree) extends Tree(Some(left), Some(right), Some(OrOperator), None) {
  override def evaluate(implicit known: Map[String, Boolean]): Option[Boolean] = left.evaluate.orElse(right.evaluate)
}

case class Not(tree: Tree) extends Tree(Some(tree), None, Some(NotOperator), None) {
  override def evaluate(implicit known: Map[String, Boolean]): Option[Boolean] = tree.evaluate.map(!_)
}

case class Symbol(symbol: String) extends Tree(None, None, None, Some(symbol)) {
  override def evaluate(implicit known: Map[String, Boolean]): Option[Boolean] = known.get(symbol)
}

sealed abstract class Operator(name: String)

case object AndOperator extends Operator("&&")

case object OrOperator extends Operator("||")

case object NotOperator extends Operator("!")
