package com.kubukoz.inference

import scala.annotation.tailrec
import scala.io.Source
import scala.languageFeature.postfixOps
import scala.util.Try

object Main {

  def wnioskujBack(s: String, rules: Set[Rule])(implicit known: Map[String, Boolean]): Option[(String, Boolean)] = {
    @tailrec
    def solve(rule: Rule)(implicit known: Map[String, Boolean]): Option[(String, Boolean)] = {
      val missingSymbols = rule.tree.symbols.filterNot(known.keySet.contains)
      missingSymbols match {
        case Nil =>
          rule.tree.evaluate map { eval =>
            (rule.result, eval)
          }
        case _ =>
          val newKnown = known ++ missingSymbols.flatMap(wnioskujBack(_, rules)).toMap
          solve(rule)(newKnown)
      }
    }

    rules.find(_.result == s) flatMap { rule =>
      println(s"Solving back using rule $rule")
      solve(rule)
    }
  }


  def wnioskujForth(s: String, rules: Set[Rule])(implicit known: Map[String, Boolean]): Option[(String, Boolean)] =
    rules.find { rule =>
      rule.tree.evaluate.isDefined
    }.flatMap { rule =>
      println(s"Solving forth using rule $rule")
      val value = rule.tree.evaluate.get
      rule.result match {
        case target if target == s => Some(s -> value)
        case target => wnioskujForth(s, rules - rule)(known + (target -> value))
      }
    }

  @tailrec
  def merge(node1: Node, nodes: Set[Node]): Option[Node] = {
    def mergeTwo(one: Node, two: Node): Node = {

      val (master, branch) = one isDeepParentOf two match {
        case true => (one, two)
        case _ => (two, one)
      }

      master.updatedChild(branch)
    }
    nodes.find(n => n.isDeepParentOf(node1) || node1.isDeepParentOf(n)) match {
      case Some(one) => merge(mergeTwo(one, node1), nodes - one)
      case None if nodes.isEmpty => Some(node1)
      case None => None
    }
  }

  implicit class RuleString(s: String) {

    def getTree(head: String): Tree = head.split("\\|\\|", 2).map(_.trim) match {
      case Array(one, two) => Or(getTree(one), getTree(two))
      case _ => head.split("&&", 2).map(_.trim) match {
        case Array(one, two) => And(getTree(one), getTree(two))
        case _ => head.split("!", 2).map(_.trim) match {
          case Array(_, two) => Not(getTree(two))
          case _ => Symbol(head)
        }
      }
    }

    def toRule: Rule = s.split("=>").map(_.trim) match {
      case split =>
        Rule(getTree(split.head), split.last)
    }
  }

  def main(args: Array[String]) {
    val argsList = args.toList
    val rulesFile = Try(argsList.head).getOrElse("rules.txt")
    val inputFile = Try(argsList(1)).getOrElse("input.txt")

    val rules = Source.fromFile(rulesFile).getLines.map(_.toRule).toSet

    val graphOpt = merge(rules.head.toNode, rules.tail.map(_.toNode))
    val rulesNotCircular = graphOpt.exists(!_.isCircular)

    if (rulesNotCircular) {
      implicit val known = Source.fromFile(inputFile).getLines.map { line =>
        line.split("=").map(_.trim) match {
          case Array(left, right) => (left, if (right == "T") true else false)
        }
      }.toMap

      val solvable = graphOpt.exists(_.isComplete)

      if (solvable) {
        println(wnioskujBack("E", rules))
        println(wnioskujForth("E", rules))
      }
      else println("Graph incomplete")
    } else println("Graph is circular")
  }
}

case class Node(name: String, children: Node*) {
  def updatedChild(branch: Node): Node = {
    children.find(_ hasSameName branch) match {
      case Some(child) => Node(name, children.map {
        case node if node hasSameName branch => branch
        case node => node
      }: _*)
      case None => Node(name, children.map(_.updatedChild(branch)): _*)
    }
  }

  def hasSameName(n: Node) = name == n.name

  def isDeepParentOf(n: Node): Boolean =
    children.exists(n hasSameName) ||
      children.exists(_ isDeepParentOf n)

  def isCircular = this.isDeepParentOf(this) || children.exists(s => s isDeepParentOf s)

  override def toString: String = s"$name${if (children.nonEmpty) "->" + children.mkString("(", ", ", ")") else ""}"

  def leaves: Seq[Node] = children.flatMap {
    case child if child.children.isEmpty => List(child)
    case child => child.leaves
  }

  def isComplete(implicit known: Map[String, Boolean]) = leaves map (_.name) forall known.keySet.contains
}

case class Rule(tree: Tree, result: String) {
  def toNode = Node(result, tree.toNodes: _*)
}
